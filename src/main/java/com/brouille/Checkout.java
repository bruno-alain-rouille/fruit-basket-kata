package com.brouille;

import com.brouille.core.Cart;
import com.brouille.core.Store;
import com.brouille.ui.CLIPrinter;

public final class Checkout {
    private Checkout() {
    }

    /**
     * Checkout the value of a cart.
     * @param args The arguments of the program.
     */
    public static void main(String[] args) {
        System.out.println("--- BEGINNING OF THE KATA ---");

        Store store = Store.getInstance();

        // Initializing the cart
        Cart cart = new Cart();
        cart.modifyAmount("Apple", 4);
        cart.modifyAmount("Orange", 3);
        cart.modifyAmount("Watermelon", 5);

        // Print cart information
        System.out.println(CLIPrinter.printCartDetail(cart));

        // Computing the value of the cart
        Float value = cart.checkout(store);

        System.out.println("This cart has a total value of " + value + "£.");
        System.out.println("--- END OF THE KATA ---");
    }
}
