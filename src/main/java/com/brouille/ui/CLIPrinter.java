package com.brouille.ui;

import java.util.Iterator;

import com.brouille.core.Cart;

public class CLIPrinter {

    public CLIPrinter() {}

    /**
     * Build the string that describes the given cart.
     * 
     * @param cart The cart that will be detailed
     * @return The string containing the cart description
     */
    public static String printCartDetail(Cart cart) {
        StringBuilder cartDetail = new StringBuilder("Your cart contains ");
        Iterator<String> iterator = cart.getItems().keySet().iterator();

        if (!iterator.hasNext()) {
            cartDetail.append("no item.");
        }

        while (iterator.hasNext()) {
            String currentKey = iterator.next();

            if (!iterator.hasNext() && cart.getItems().keySet().size() > 1) {
                cartDetail.deleteCharAt(cartDetail.length() - 2);
                cartDetail.append("and ");
            }

            cartDetail.append(cart.getItems().get(currentKey) + " " + currentKey.toLowerCase());

            if (cart.getItems().get(currentKey) > 1) {
                cartDetail.append("s");
            }

            if (iterator.hasNext()) {
                cartDetail.append(", ");
            } else {
                cartDetail.append(".");
            }
        }
        return cartDetail.toString();
    }
}