package com.brouille.core;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class Store {

    private static Store instance = new Store();

    public Map<String, Float> catalog;
    public Map<String, Function<Integer, Integer>> offers;

    private Store() {
        catalog = new HashMap<>();
        catalog.put("Apple", 0.2f);
        catalog.put("Orange", 0.5f);
        catalog.put("Watermelon", 0.8f);

        offers = new HashMap<>();
        offers.put("Apple", amount -> (amount / 2) + (amount % 2));
        offers.put("Watermelon", amount -> (amount / 3)*2 + (amount % 3));
    }

    /**
     * Obtains the price of the specified item.
     * 
     * @param item The name of the item
     * @return A float that equals the price of the items
     * @exception IllegalArgumentException If the requested item is not in store.
     */
    public Float getPrice(String item) {
        if (catalog.get(item) == null) {
            throw new IllegalArgumentException("Unknown product");
        } else {
            return catalog.get(item);
        }
    }

    /**
     * Obtains the function that symbolizes the offer by returning the equivalent amount of specified items after the offer is applied.
     * 
     * @param item The name of the item
     * @return A function representing an offer
     */
    public Function<Integer, Integer> getOffer(String item) {
        if (offers.get(item) == null) {
            return amount -> amount;
        } else {
            return offers.get(item);
        }
    }

    public static Store getInstance() {
        return instance;
    }
}