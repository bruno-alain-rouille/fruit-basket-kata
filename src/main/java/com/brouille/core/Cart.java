package com.brouille.core;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

public class Cart {

    private Map<String, Integer> items;
    private final int SCALE = 2;

    public Cart() {
        items = new HashMap<>();
    }

    public Cart(Map<String, Integer> items) {
        this.items = items;
    }

    /**
     * Increase or decrease the amount of a specified item in the cart.
     * 
     * @param item The item which amount will be modify
     * @param amount The value of the modification
     */
    public void modifyAmount(String item, Integer amount) {
        
        Integer initialAmount = this.getItems().get(item);
        
        if (initialAmount == null) {
            if (amount > 0) {
                this.getItems().put(item, amount);
            }
        } else if (initialAmount + amount <= 0) {
            this.getItems().remove(item);
        } else {
            this.getItems().replace(item, initialAmount + amount);
        }
    }

    /**
     * Compute the total value of a cart by applying the available offers.
     * 
     * @param store The Store that contains the prices and the available offers.
     */
    public Float checkout(Store store) {
        
        Float totalValue = 0.0f;
        
        Integer amount;
        Float price;
        for (String item : this.getItems().keySet()) {
            amount = store.getOffer(item).apply(this.getItems().get(item));
            price = store.getPrice(item);
            totalValue += amount * price;
        }

        return round(totalValue, SCALE);
    }

    /**
     * Round up a float value according to a specified scale.
     * 
     * @param value The float to round.
     * @param scale The desired scale to round with.
     * @return The rounded float.
     */
    protected Float round(Float value, int scale) {
        BigDecimal bd = new BigDecimal(Float.toString(value));
        bd = bd.setScale(scale, RoundingMode.HALF_UP);
        return bd.floatValue();
    }

    /**
     * @return Map<String, Integer> return the items
     */
    public Map<String, Integer> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(Map<String, Integer> items) {
        this.items = items;
    }

}