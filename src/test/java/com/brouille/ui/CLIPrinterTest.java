package com.brouille.ui;

import static org.junit.Assert.assertEquals;

import com.brouille.core.Cart;

import org.junit.Before;
import org.junit.Test;

public class CLIPrinterTest {

    private Cart cart;

    @Before
    public void setUp() {
        cart = new Cart();
    }

    @Test
    public void testPrintCartDetail() {
        cart.modifyAmount("Apple", 4);
        cart.modifyAmount("Orange", 3);
        cart.modifyAmount("Watermelon", 5);

        String expectedString = "Your cart contains 4 apples, 5 watermelons and 3 oranges.";

        assertEquals(expectedString, CLIPrinter.printCartDetail(cart));

        cart.modifyAmount("Apple", -3);
        cart.modifyAmount("Orange", -2);
        cart.modifyAmount("Watermelon", -5);

        expectedString = "Your cart contains 1 apple and 1 orange.";

        assertEquals(expectedString, CLIPrinter.printCartDetail(cart));

        cart.modifyAmount("Apple", -1);
        cart.modifyAmount("Orange", -1);

        expectedString = "Your cart contains no item.";

        assertEquals(expectedString, CLIPrinter.printCartDetail(cart));
    }
}