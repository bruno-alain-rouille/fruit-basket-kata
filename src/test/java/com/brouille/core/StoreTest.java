package com.brouille.core;

import static org.junit.Assert.assertEquals;

import java.util.function.Function;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class StoreTest {

    private Store classToTest;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        classToTest = Store.getInstance();
    }

    @Test
    public void testGetPrice() {
        String item = "Apple";

        assertEquals(Float.valueOf(0.2f), classToTest.getPrice(item));

        item = "Orange";

        assertEquals(Float.valueOf(0.5f), classToTest.getPrice(item));

        item = "Watermelon";

        assertEquals(Float.valueOf(0.8f), classToTest.getPrice(item));
    }
    
    @Test
    public void testGetPriceWithIllegalArgument() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Unknown product");
        
        String item = "Anything else";

        classToTest.getPrice(item);
    }

    @Test
    public void testGetOffer() {
        String item = "Apple";
        Function<Integer, Integer> offer = classToTest.getOffer(item);

        assertEquals(Integer.valueOf(0), offer.apply(0));
        assertEquals(Integer.valueOf(5), offer.apply(10));
        assertEquals(Integer.valueOf(6), offer.apply(11));

        item = "Watermelon";
        offer = classToTest.getOffer(item);

        assertEquals(Integer.valueOf(0), offer.apply(0));
        assertEquals(Integer.valueOf(7), offer.apply(10));
        assertEquals(Integer.valueOf(8), offer.apply(11));

        item = "Anything else";
        offer = classToTest.getOffer(item);

        assertEquals(Integer.valueOf(0), offer.apply(0));
        assertEquals(Integer.valueOf(10), offer.apply(10));
        assertEquals(Integer.valueOf(11), offer.apply(11));
    }
}