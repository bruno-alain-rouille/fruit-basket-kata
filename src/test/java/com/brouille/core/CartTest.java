package com.brouille.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class CartTest {

    private Cart classToTest;
    private Store store;

    @Before
    public void setUp() {
        classToTest = new Cart();
        store = Store.getInstance();
    }

    @Test
    public void testModifyAmount() {
        Map<String, Integer> cart = new HashMap<>();
        cart.put("Apple", 4);
        cart.put("Orange", 3);
        cart.put("Watermelon", 5);
        classToTest.setItems(cart);

        classToTest.modifyAmount("Apple", 1);
        classToTest.modifyAmount("Orange", 2);
        classToTest.modifyAmount("Watermelon", 3);
        classToTest.modifyAmount("Anything else", 4);

        assertEquals(Integer.valueOf(5), classToTest.getItems().get("Apple"));
        assertEquals(Integer.valueOf(5), classToTest.getItems().get("Orange"));
        assertEquals(Integer.valueOf(8), classToTest.getItems().get("Watermelon"));
        assertEquals(Integer.valueOf(4), classToTest.getItems().get("Anything else"));

        classToTest.modifyAmount("Apple", -1);
        classToTest.modifyAmount("Orange", -2);
        classToTest.modifyAmount("Watermelon", -3);
        classToTest.modifyAmount("Anything else", -5);

        assertEquals(Integer.valueOf(4), classToTest.getItems().get("Apple"));
        assertEquals(Integer.valueOf(3), classToTest.getItems().get("Orange"));
        assertEquals(Integer.valueOf(5), classToTest.getItems().get("Watermelon"));
        assertNull(classToTest.getItems().get("Anything else"));

        classToTest.getItems().clear();
        classToTest.modifyAmount("Apple", -1);

        assertTrue(classToTest.getItems().isEmpty());
    }

    @Test
    public void testRound() {
        Float valueToRound = 15.5964f;

        assertEquals(Float.valueOf(15.60f), classToTest.round(valueToRound, 2));
        
        valueToRound = 15.5964f;
        
        assertEquals(Float.valueOf(15.596f), classToTest.round(valueToRound, 3));
        
        valueToRound = 15.5944f;

        assertEquals(Float.valueOf(15.59f), classToTest.round(valueToRound, 2));
    }

    @Test
    public void testCheckout() {
        Map<String, Integer> cart = new HashMap<>();
        cart.put("Apple", 4);
        cart.put("Orange", 3);
        cart.put("Watermelon", 5);
        classToTest.setItems(cart);

        assertEquals(Float.valueOf(5.1f), classToTest.checkout(store));
    }

}